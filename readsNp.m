
function obj=readsNp(fullname)
%
% Read networks data from a touchstone file (.sNp)
%
%   data=READSNP('Filename') parse data from the touchstone 'Filename'.
%
% The data argument is a structure with the following fields:
%   Name:   name of the touchstone file without path and extension.
%   Zref:   reference impedance
%   Freq:   frequency, in the units specified on the option line.
%   Data:   scattering matrix of size NxNxlength(Freq)
%   Type:   type of parameters [S|Y|Z|G|H]
%   Noise:  structure with the following fields:
%
%            Freq:   Frequency, in the units specified on the option line.
%            Nfig:   Minimum noise figure in decibels (dB).
%            Sref:   Source reflection to realize minimum noise figure.
%            Nres:   Effective noise resistance.
%
% NOTE: MIXEDMODE data still NOT SUPPORTED.
%
% Comments on data arranging for n-port networks in Version 2 files follow.
% From Touchtone File Format Specification (Copyright 2009 by TechAmerica):
%
% Rules for Version 2.0:
% In Version 2.0 files, the data associated with any one frequency may be
% split across any number of lines or may be placed on a single line of
% arbitrary length. Network data in a Version 2.0 file is parsed using the
% [Number of Ports] keyword and argument and the [Matrix Format] keyword
% and argument.
%
% For a Full matrix, a new frequency point is expected every 2n^2 + 1
% values, % where n is the number of ports, regardless of intervening line
% termination sequences or characters.  For a Lower or Upper matrix, a new
% frequency point is expected every n^2 + n + 1 values.
%
%
% Structure of the touchstone files (text after ! are comments):
%
% =========================================================================
% =========================================================================
%
% [Version] 2.0                   !required for v2. consider v1.1 otherwise
% # (option line)                 !required
%
% ! For 1-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
% ! For 2-port files: # [Hz|kHz|MHz|GHz] [S|Y|Z|G|H] [DB|MA|RI] [R n]
% ! For 3-port and beyond: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
% ! For mixed-mode files: # [Hz|kHz|MHz|GHz] [S|Y|Z] [DB|MA|RI] [R n]
%
% ! Note that H- and G-parameters are defined for 2-port networks only.
% ! These hybrid parameters cannot be used to describe networks containing
% ! any other number of ports.
%
% ! For touchstone versions below v2 all the following flag are ignored:
%
% [Number of Ports]               !required for v2.
% [Two-Port Order]                !required if a 2-port system is described
% [Number of Frequencies]         !required
% [Number of Noise Frequencies]   !required if [Noise Data] defined
% [Reference]                     !optional
% [Matrix Format]                 !optional
% [Mixed-Mode Order]              !optional
% [Begin Information]/[End Information]   !(optional
%
% [Network Data]                     !beginning of network data
%
% !Data in row format (for 1-port or 2-ports networks)
% !   -------------------------------------------------
%     f s11.1 s11.2 s21.1 s21.2 s12.1 s12.2 s22.1 s22.2
% !   -------------------------------------------------
%
% !Data in matrix format (for 3-ports networks and above)
% !    -------------------------------------------------
%      f s11.1 s11.2 s12.1 s12.2 ... s1x.1 s1N.2,...
%        s21.1 s21.2 s22.2 s22.2 ... s2x.1 s2N.2,...
%        ...',...
%        sN1.1 sN1.2 sN2.1 sN2.2 ... sNN.1 sNN.2,...
% !    -------------------------------------------------
%
% [Noise Data]   !required only if [Number of Noise Frequencies] given
%
% ! Noise data is place after the [Noise Data] flag in five column format:
%
%  <frequency> <x1> <x2> <x3> <x4>
%
% [End]          !end of the file
%
% =========================================================================
% =========================================================================
%
% 28.12.2016, Function readsNp version 3:  Added support for touchstone
% format Version 2.0 (files starting with the flag [Version] 2.0)
%
% Function readsNp version 2: Read data from a touchstone file v1.1
% (not working with a touchstone v2) to construct a "touchstone" object.
%
%
% 
% 
% 
% 
% 
% See also: WRITESNP
%
% Author: David Martinez <david.martinez@inria.fr>



%% Define flags for touchstone format v2
flag_ver='[Version]';
flag_np='[Number of Ports]';
flag_porder='[Two-Port Order]';
flag_nf='[Number of Frequencies]';
flag_nn='[Number of Noise Frequencies]';
flag_ref='[Reference]';
flag_matformat='[Matrix Format]';
flag_mixedmode='[Mixed-Mode Order]';
flag_begininf='[Begin Information]';
flag_endinf='[End Information]';
flag_data='[Network Data]';
flag_noise='[Noise Data]';
flag_end='[End]';


%% Get filename and extension, this one contains the number of ports N
 if nargin==0
     [filename, pathname, ~] = uigetfile( ...
         {'*.s?p', 'Touchstone files (*.s1p, *.s2p, *.s3p, ...)';
         '*.*',  'All Files (*.*)'}, ...
         'A touchstone file is needed...');
     if isequal(filename,0) || isequal(pathname,0)
         disp('User pressed cancel');
         return;
     else
         disp(['User selected ', filename]);
         fullname=fullfile(pathname,filename);
     end
 end


[filepath, filename, extension] = fileparts(fullname);
if isempty(extension)
    matches=dir(fullfile(filepath,[filename '*']));
    switch length(matches) 
        case 0
            error('file not found')
        case 1
            
        otherwise
            warning(['There are multiple matches with the same filename, using "' matches(1).name '"'])
    end
    [~,filename,extension]=fileparts(matches(1).name);
end


%% Get the port number, coded in the file extension
N=str2double(extension(3:end-1));
if isnan(N) || isempty(N)
    error('Unsupported extension');
end

%% Read full file and save it to a string array. Close file afterwards.
text = fileread(fullfile(filepath,[filename extension]));

%% read first line
% In the touchstone v1.1 the option line should be the first uncommented line
% In touchstone v2 the option line come after the flag '[Version] 2.0'
[firstline,text] =readline(text);

%% seek [Version] flag
% If version flag is not present, version 1 is considered
Vpos=strfind(firstline, flag_ver); % seek version flag
if ~isempty(Vpos)
    version=str2double(firstline(Vpos+length(flag_ver):end));
    [options,text]=readline(text);
else
    version=1; % [Version] flag not found. consider version 1
    options=firstline;
end

%% check option
if ~strncmp('#',options,1)
    error('Option line or [Version] flag not found.')
end

%% Convert option line to uppercase string
cline = upper(strtrim(options));

% NOTE: Parameter in the option line does not follow a given order (appart
% from the # mark at the beginning and the impedance at the end. It makes
% necessary to search for them since we do not know were they are a priori.

%% Check touchstone type 
t = regexp(cline,'\s(?<type>[SYZHGT])\s','names');
switch length(t)
    case 0
        type = 'S';
    case 1
        type = t.type;
    otherwise
        error('multiple type statements found in the options line')
end
    
    
%%  Find the frequency scaling factor.
t = regexp(cline,'\s(?<scale>[GMK]*HZ)\s','names');
switch length(t)
    case 0
        FScale = 'GHZ';
    case 1
        FScale = t.scale;
    otherwise
        error('multiple frequency statements found in the options line')
end
switch FScale
    case 'GHZ'
        FScale = 1e9;
    case 'MHZ'
        FScale = 1e6;
    case 'KHZ'
        FScale = 1e3;
    case 'HZ'
        FScale = 1;
    otherwise
        error('This is not possible')
end

%% Find the format of network parameters from a MATLAB string.
t = regexp(cline,'\s(?<format>(RI)|(MA)|(DB))\s','names');
switch length(t)
    case 0
        FScale = 'MA';
    case 1
        DataFormat = t.format;
    otherwise
        error('multiple format statements found in the options line')
end

%% Find Z0.
t = regexp(cline,'\sR\s(?<refVal>[\-\+0-9e\.]+)','names');
switch length(t)
    case 0
        Z_0 = 50;
    case 1
        Z_0 = str2double(t.refVal);
    otherwise
        error('multiple reference impedance statements found in the options line')
end

%% Initialize flags
porder='21_12';
matformat='Full';
nfp=[];
nnf=[];

%% Read additional flags (only for touchstone version 2 and above)
if version >=2
    [flagline,text]=readline(text);
    while ~contains(flagline,flag_data)
        [flag,pos]=regexp(flagline,'([).*(])','match'); % find text within []
        if ~isempty(flag)
            flag=flag{1};
            % the following flags may appear in any order relative to each other.
            switch flag
                case    flag_np % Number of ports
                    
                    str=flagline(pos+length(flag_np):end);
                    N=uint8(str2double(str));% 8 bit number for number of ports
                    if ~(N>0)
                        error(['Value of ',flag_np,' not valid.']);
                    end
                    
                case    flag_porder % ports order
                    
                    str=flagline(pos+length(flag_porder):end);
                    if contains(str, '12_21')
                        porder = '12_21';
                    end
                    
                case    flag_nf % number of frequencies
                    
                    str=flagline(pos+length(flag_nf):end);
                    nfp=uint32(str2double(str)); % 32 bit number for nfp
                    if ~(nfp>0)
                        error(['Value of ',flag_nf,' not valid.']);
                    end
                    
                case    flag_nn % number of noise frequencies
                    
                    str=flagline(pos+length(flag_nn):end);
                    nnf=uint32(str2double(str)); % 32 bit number for nfp
                    
                case    flag_ref % Reference impedance (Note that [Reference]
                    % flag and its arguments may span multiple lines
                    
                    str=flagline(pos+length(flag_ref):end);
                    z0=textscan(str,'%s');
                    z0=str2double(z0{:});
                    if length(z0)<N     % Not enough reference values after the flag, search in next lines
                        z2 = textscan(fid, '%s', N-length(z0), 'delimiter', {'\t','\n',' '},...
                            'CommentStyle', '!','MultipleDelimsAsOne',1, 'whitespace', '');
                        z0=[z0; str2double(z2{:})]; %#ok<AGROW>
                    end
                    if any(~(z0>0)) || any(~isfinite(z0))
                        error(['Value of ',flag_ref,' not valid.']);
                    else
                        Z_0=z0;
                    end
                    
                case    flag_matformat % Matrix format. Default: Full
                    
                    str=flagline(pos+length(flag_matformat):end);
                    if contains(str, 'Lower')
                        matformat = 'Lower';
                    elseif contains(str, 'Upper')
                        matformat = 'Upper';
                    end
                    
            end
        end
        [flagline,text]=readline(text);
    end
end


%% Compute the number of data fields for each frequency (data may expand several lines)
% For a Full matrix, a new frequency point is expected every 2n^2 + 1 values,
% where n is the number of ports, regardless of intervening line termination
% sequences or characters.  For a Lower or Upper matrix, a new frequency
% point is expected every n^2 + n + 1 values.
switch matformat
    case 'Full'
        Col = 2*N^2 + 1;
    case {'Lower','Upper'}
        Col = N^2 + N + 1;
    otherwise
        error('Unknown matrix format.');
end

%% Read full data (it is necessary to ignore all possible extra lines with !)
[DATA,text] = readdata(text);

Col=double(Col);
freq=DATA(1:Col:end,1);
%% Read Noise data (if necessary)
NOISE=[];
if version==1   % In version 1 noise data starts where the frequency decreases for the first time
    ind=find(freq(1:end-1)>freq(2:end));
    if ~isempty(ind)
        nfp=ind(1);
        freq=freq(1:nfp);
        NOISE=DATA(Col*nfp+1:end);
        DATA(Col*nfp+1:end)=[];
    end
elseif N==2 && ~isempty(nnf)
    while isempty(NOISE) && ~isempty(text)
        [noise_header,text] = readline(text);
        if contains(noise_header, flag_noise)
            [NOISE,text] = readdata(text);
        end
    end
end

%% Process network parameters
% Compute data with the correspondent units
DATA(1:Col:end)=[];   % Remove frequency values
switch DataFormat
    case 'RI'
        re=DATA(1:2:end); % Real part of S-parameters
        im=DATA(2:2:end); % Imaginary part of S-parameters
        S_Param=re+1i*im;
    case 'MA'
        module=DATA(1:2:end); % Module of S-parameters (linear)
        phase=DATA(2:2:end); % Phase of S-parameters (degrees)
        S_Param=module.*exp(1i*phase*pi/180);
    case 'DB'
        module=DATA(1:2:end); % Module of S-parameters (decibels)
        phase=DATA(2:2:end); % Phase of S-parameters (degrees)
        S_Param=10.^(module/20).*exp(1i*phase*pi/180);
end

%% Complete data if upper or lower triangular matrix and reshape
%% scattering parameters into a 3d matrix of dimension NxNxnfp
switch matformat
    case 'Full'
        Param_3D=reshape(S_Param,N,N,nfp);
    case 'Lower'
        U=repmat(triu(ones(N),0),[1,1,nfp]); % Initialize 3d triangular matrix
        U(U==1)=S_Param;                     % Fill upper triangular matrix
        L=repmat(triu(ones(N),1),[1,1,nfp]); % Compute index to mirror upper triangular matrix
        Param_3D=U+permute(U(L==1),[2,1,3]); % Complete 3d matrix
    case 'Upper'
        L=repmat(tril(ones(N),0),[1,1,nfp]); % Initialize 3d triangular matrix
        L(L==1)=S_Param;                     % Fill lower triangular matrix
        U=repmat(tril(ones(N),-1),[1,1,nfp]); % Compute index to mirror lower triangular matrix
        Param_3D=L+permute(L(U==1),[2,1,3]); % Complete 3d matrix
end


%% Change parameters order unles flag_porder = '21_12' for .s2p file.
if (N==2)
    if strcmp(porder,'12_21')
        Param_3D=permute(Param_3D,[2,1,3]);
    end
else
    Param_3D=permute(Param_3D,[2,1,3]);
end

%% Process noise data
NOISE=reshape(NOISE,5,[]).';

%% In touchstone format version 2, check that the data size corresponds to
%% the number of frequency points (nfp) and the number of noise frequencies (nnf)
%% (this information is not really useful)

if version~=1
    if nfp~=size(Param_3D,3)
        warning(['readsNp: Number of frequency points in the file does not match the value after the flag ',flag_nf]);
    end
    if ~isempty(nnf) && nnf~=size(NOISE,1)
        warning(['readsNp: Number of noise frequencies in the file does not match the value after the flag ',flag_nn]);
    end
end

%% assign output arguments
obj.Name=filename;
obj.Zref=Z_0;
obj.Freq = freq*FScale;
obj.Data=Param_3D;
obj.Type=type;
if ~isempty(NOISE)
    obj.Noise=struct('Freq',NOISE(:,1),...
                     'Nfig',NOISE(:,2),...
                     'Sref',10.^(NOISE(:,3)/20).*exp(1i*NOISE(:,4)*pi/180),...
                     'Nres',NOISE(:,4));
end
end


function [line,text]=readline(text)
[line,position] = textscan(text, '%s', 1, 'delimiter', {'\n'},...
    'CommentStyle', '!','MultipleDelimsAsOne',1, 'whitespace', '');
line = line{1}{1};
text = extractAfter(text,position);
end

function [DATA,text] = readdata(text)
[DATA,position] = textscan(char(text),'%f','CommentStyle', {'!'}, ...
    'delimiter', {' ','\r\n','\n'}, 'MultipleDelimsAsOne',1, 'WhiteSpace', '');
DATA=DATA{1};
text = extractAfter(text,position);
end

