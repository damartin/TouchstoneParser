function run_Touchstone_tests()

% go to the right folder
fi = mfilename('fullpath');
path = fileparts([fi,'.m']);
cd(path)
cd ..

import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin

% add the tested folders to the list of folders to profile
codeFolders = {fullfile(pwd)};

% generate the test suite
suite = TestSuite.fromFile(fullfile('tests','testTouchstoneParser.m'));

% generate a runner with a code coverage plugin
runner = TestRunner.withTextOutput;
runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));

% run the test suite
result = runner.run(suite);

end

