classdef testTouchstoneParser < matlab.unittest.TestCase
    
    properties (TestParameter)
        % Representations
        Type = struct('S','S','Z','Z','Y','Y','G','G','H','H','T','T');
        % F is the amount of frequencies
        F = struct('small', 1,'medium', 10, 'large', 100);
        % P is the number of ports
        P = struct('small', 1,'medium', 2, 'large', 10);
        Ref=struct('single','single','diff','diff')
        Version = struct('one',1,'two',2)
        Format = {'MA','DB','RI'};
        Scale={'HZ','KHZ','MHZ','GHZ'};
    end
    
    methods (Test)
        function testDefaultParameters(testCase,P,F)
            % generate some random data
            Data = rand(P,P,F);
            Freq = linspace(0,1e9,F);
            % write the data to a file
            writesNp(Data,Freq);
            % read the data back into matlab
            RES = readsNp('Data');
            delete('Data*')
            % res now contains the following fields. 
            %   obj.Name=filename;
            %   obj.Zref=Z_0;
            %   obj.Freq = freq*FScale;
            %   obj.Data=Param_3D;
            %   obj.Type=type;
            %   obj.Noise=NOISE;
            % Do some checking...
            testCase.verifyEqual(RES.Freq(:),Freq(:),'reltol',1e-6);
            testCase.verifyEqual(RES.Data,Data,'reltol',1e-6);
        end
        function testAllParameters(testCase,P,F,Ref,Version,Format,Type,Scale)
            % skip the test if the amount of ports is not 2 for the special
            % types of representation
            if P~=2
                if ~ismember(Type,'SYZ')
                    return
                end
            end
            % generate some random data
            Data = rand(P,P,F);
            Freq = linspace(0,1e9,F);
            % generate the vector of reference impedances
            switch Ref
                case 'single'
                    Ref = 50;
                case 'diff'
                    Ref = 100*rand(P,1);
            end
            % write the data to a file
            writesNp(Data,Freq,'Name','adam','Ref',Ref,'Version',Version,'Format',Format,'Type',Type,'Scale',Scale);
            % read the data back into matlab
            RES = readsNp('adam');
            delete('adam*')
            % res now contains the following fields. 
            %   obj.Name=filename;
            %   obj.Zref=Z_0;
            %   obj.Freq = freq*FScale;
            %   obj.Data=Param_3D;
            %   obj.Type=type;
            %   obj.Noise=NOISE;
            % Do some checking...
            testCase.verifyEqual(RES.Freq(:),Freq(:),'reltol',1e-6);
            testCase.verifyEqual(RES.Data,Data,'reltol',1e-6);
            testCase.verifyEqual(RES.Type,Type);
        end
    end
end

