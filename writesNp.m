
function T = writesNp(varargin)


% WRITESNP   Write data to a touchstone file v1.1
%
%   WRITESNP(obj) write structure data to a file .sNp
%
%   WRITESNP(obj,name) write structure data to a file name.sNp
%
%   WRITESNP(obj,name,'Name',name) write structure data to a file name.sNp
%   and format the scattering parameters according to type. Argument
%   type accept the following options: MA for module and phase, RI for
%   real and imaginary part, DB for module in dB and phase in deg.
%
%   Optional input argument are:
%
%   Version: specifies if the touchstone format corresponds to Version 1 or
%   2. If not specified p.Results.Version 1 is considered.
%
%   commentline: comment to be writen at the begining of the  file.
%
%
%
% 
% 
% 
% 
% 
%   See also: WRITESNP
%
%   Author: David Martinez <david.martinez@inria.fr>

%% Parse input parameters
validVersions={'1','2'};
validFormats={'MA','DB','RI'};
validTypes={'S','Y','Z','G','H','T'};
validScales={'HZ','KHZ','MHZ','GHZ'};
p=inputParser;
p.addRequired('Data',@isnumeric);
p.addRequired('Freq',@(x) isvector(squeeze(x)));
p.addParameter('Noise',1,@ismatrix);
p.addParameter('Ref',50,@isvector);
p.addParameter('Name',inputname(1),@ischar);
p.addParameter('Version',1,@(x) ismember(num2str(x),validVersions));
p.addParameter('Comment','',@ischar);
p.addParameter('Format','RI',@(x) ismember(upper(x),validFormats));
p.addParameter('Type','S',@(x) ismember(upper(x),validTypes));
p.addParameter('Scale','GHZ',@(x) ismember(upper(x),validScales));
p.StructExpand = 1;
p.parse(varargin{:});


%% Define flags for touchstone format v2
flag_ver='[Version]';
flag_np='[Number of Ports]';
flag_porder='[Two-Port Order]';
flag_nf='[Number of Frequencies]';
flag_nn='[Number of Noise Frequencies]';
flag_ref='[Reference]';
flag_matformat='[Matrix Format]';
flag_mixedmode='[Mixed-Mode Order]';
flag_begininf='[Begin Information]';
flag_endinf='[End Information]';
flag_data='[Network Data]';
flag_noise='[Noise Data]';
flag_end='[End]';

%% Get data
args=p.Results;

%% Number of ports
N=size(args.Data,1);

% if it's a TwoPort and we use version 1, the order is 21_12, so we need to permute
if (N==2)&&(args.Version==1)
    args.Data = permute(args.Data,[2,1,3]);
end

%% Check number of impedance values if a vector is provided
nref = length(args.Ref(:));
if nref>1 && (nref~=N)
    error('Number of reference impedance does not match the number of ports');
end
%% Check format of noise data
if ~isempty(args.Noise)
    if N~=2
        error('Noise data is only allowed for 2-ports networks');
    end
    if size(args.Noise,2)~=5
        error('Noise data must be formmated in 5 columns');
    end
    if args.Version == 1 && (args.Noise(1,1) >= args.Freq(end))
        error('First noise frequency must be smaller than last data frequency in touchstone v1');
    end
end

%% filename
args.Name=[args.Name,'.s',num2str(N),'p'];

%% Preallocate touchstone data
data=NaN(length(args.Freq),1+2*N^2);

%% Format frequency axis
switch args.Scale
    case 'GHZ'
        data(:,1)=args.Freq(:)*1e-9;
    case 'MHZ'
        data(:,1)=args.Freq(:)*1e-6;
    case 'KHZ'
        data(:,1)=args.Freq(:)*1e-3;
    case 'HZ'
        data(:,1)=args.Freq(:);
    otherwise
        error('scale unknown')
end

%% Scale noise
if ~isempty(args.Noise)
    switch args.Scale
        case 'GHZ'
            args.Noise(:,1) = args.Noise(:,1)*1e-9;
        case 'MHZ'
            args.Noise(:,1) = args.Noise(:,1)*1e-6;
        case 'KHZ'
            args.Noise(:,1) = args.Noise(:,1)*1e-3;
        case 'HZ'
            
        otherwise
            error('scale unknown')
    end
end

%% Format scattering parameters with the indicated type
args.Data = permute(args.Data,[3,2,1]);
args.Data = reshape(args.Data,[],N.^2);
args.Data = double(args.Data);
switch p.Results.Format
    case 'MA'
        data(:,2:2:end)=abs(args.Data);
        data(:,3:2:end)=angle(args.Data)*180/pi;
    case 'DB'
        data(:,2:2:end)=20*log10(abs(args.Data));
        data(:,3:2:end)=angle(args.Data)*180/pi;
    case 'RI'
        data(:,2:2:end)=real(args.Data);
        data(:,3:2:end)=imag(args.Data);
end

%% optionline
option=['# ',args.Scale,' ',args.Type,' ',args.Format,' R ',num2str(args.Ref(1))];

%% save data to a cell array
% comments
comment={['! Touchstone Version  ', num2str(args.Version)]; ...
['! Touchstone file generated at ',datestr(datetime),' by writesNp']; ...
 '! Functions "WRITESNP" and "READSNP" for matlab can be found at "https://gitlab.inria.fr/damartin/TouchstoneParser.git"'; ...
['! ',args.Comment]};
  

%% network data
if N<=2
    % Write data in row format
    %     -------------------------------------------------
    %     f s11.1 s11.2 s21.1 s21.2 s12.1 s12.2 s22.1 s22.2
    %     -------------------------------------------------
    format=repmat('%12.12f ',1,2*N^2+1);
    data_str=sprintf([format,'\n'],data.');
    
else
    % Write data in matrix format
    %     -------------------------------------------------
    %      f s11.1 s11.2 s12.1 s12.2 ... s1x.1 s1x.2,...
    %        s21.1 s21.2 s22.2 s22.2 ... s2x.1 s2x.2,...
    %        ...',...
    %        sx1.1 sx1.2 sx2.1 sx2.2 ... sxx.1 sxx.2,...
    %     -------------------------------------------------
    % Reformat data in a matrix form
    mat_data=NaN(N*length(args.Freq),2*N+1);
    mat_data(1:N:end,1)=data(:,1);
    mat_data(:,2:end)=reshape(data(:,2:end).',2*N,[]).';
    % Print mat_data to a string
    format=repmat('%12.12f ',1,2*N+1);
    data_str=sprintf([format,'\n'],mat_data.');
    % Replace 'NaN' with white spaces
    data_str = strrep(data_str, 'NaN', '   ');
end

%% Noise data
    format=repmat('%12.12f ',1,5);
    noise_str = sprintf([format,'\n'],args.Noise.');


%% add flags for p.Results.Version 2
endmark = [];
if p.Results.Version==2
    flags={[flag_np,' ',num2str(N)]; ...
        [flag_porder,' 12_21']; ...
        [flag_nf,' ',num2str(length(args.Freq))]; ...
        [flag_nn,' ',num2str(size(args.Noise,1))]};
    
    if nref>1
        flags = [flags; [flag_ref,' ',num2str(args.Ref(:).')] ];
    end
    
    option = [[flag_ver,' ',num2str(args.Version)]; ...
        option; ...
        flags];
    
    % add flags [Network Data] [Noise Data] and [End]
    data_str  = {flag_data;data_str};
    noise_str = {flag_noise;noise_str};
    endmark   = flag_end;
end


%% write to file
 CellArray = [comment;option;data_str;noise_str;endmark]; 
     fid = fopen(args.Name,'w');
        T = fprintf(fid,'%s\r\n',CellArray{:});
    fclose(fid);
end
















